#ifndef HELPER_H
#define HELPER_H
#pragma once

#include "data.h"

#include <QMessageBox>

namespace Helper{

/////////////////////////////////////////////////////QT Dialogs/////////////////////////////////////////////////////

/// \brief askUser is a simple wrapper for a QMessageBox
/// \param msg          message to display
/// \param button_1     first QMessageBox::StandardButton to show
/// \param button_2     (optional) second button
/// \param button_3     (optional) third button
/// \return             0 if user presses button_1, 1 for button_2 and 2 for button_3
///
int askUser(const QString& msg, int button_1, int button_2 = -1, int button_3 = -1){
    QMessageBox dialog;
    dialog.setText(msg);
    if(button_2 != -1){
        if(button_3 != -1){
            dialog.setStandardButtons(static_cast<QMessageBox::StandardButton>(button_1) | static_cast<QMessageBox::StandardButton>(button_2) | static_cast<QMessageBox::StandardButton>(button_3));
        }
        else{
            dialog.setStandardButtons(static_cast<QMessageBox::StandardButton>(button_1) | static_cast<QMessageBox::StandardButton>(button_2));
        }
    }
    else{
        dialog.setStandardButtons(static_cast<QMessageBox::StandardButton>(button_1));
    }
    dialog.setDefaultButton(static_cast<QMessageBox::StandardButton>(button_1));
    int answer = dialog.exec();

    if(answer == button_2){
        return 1;
    }
    else if(answer == button_3){
        return 2;
    }

    return 0;
}

///
/// \brief askUser is a simple wrapper for a QMessageBox
/// \param msg          message to display
/// \param button_1     text of first button
/// \param button_2     (optional) text of second button
/// \param button_3     (optional) text of third button
/// \return             0 if user presses button_1, 1 for button_2 and 2 for button_3
///
int askUser(const QString& msg, const QString& button_1, const QString& button_2 = "", const QString& button_3 = ""){
    QMessageBox dialog;
    dialog.setText(msg);
    dialog.addButton(button_1, QMessageBox::AcceptRole);
    if(button_2.size() != 0){
        dialog.addButton(button_2, QMessageBox::RejectRole);
    }
    if(button_3.size() != 0){
        dialog.addButton(button_3, QMessageBox::DestructiveRole);
    }
    return dialog.exec();
}

///
/// \brief dialogYesNo opens a simple yes/no dialog
/// \param msg      message to display
/// \return         true if user clicks yes, false otherwise
///
bool dialogYesNo(const QString& msg){
    return (0==askUser(msg,QMessageBox::Yes,QMessageBox::No));
}

///
/// \brief dialogOkCancel opens a simple ok/cancel dialog
/// \param msg      message to display
/// \return         true if user clicks Ok, false otherwise
///
bool dialogOkCancel(const QString& msg){
    return (0==askUser(msg,QMessageBox::Ok,QMessageBox::Cancel));
}

///
/// \brief info opens a simple dialog for user information with a OK button
/// \param msg
///
void info(const QString& msg){
    QMessageBox dialog;
    dialog.setWindowTitle("Info");
    dialog.setText(msg);
    dialog.addButton(QMessageBox::Ok);
    dialog.exec();
}

/////////////////////////////////////////////////////Parsing/////////////////////////////////////////////////////

///
/// \brief getFileType
/// \param file_name
/// \return
///
QString getFileType(const QString& file_name){
    int last_dot_idx = file_name.lastIndexOf('.');
    if(last_dot_idx != -1){
        return file_name.right(file_name.size()-last_dot_idx-1);
    }
    return "";
}

///
/// \brief analyzeSinglePart
/// \param part
/// \param type
/// \return
///
bool analyzeSinglePart(const QString& part, int* type){
    if(part.size() == 0){
        return false;
    }

    //check if string contains chars..
    bool contains_chars = false;
    for(int i=0; i < part.size(); i++){
        QChar current = part.at(i);
        if(((current>='A') & (current<='Z'))||((current>='a') & (current<='z'))){
            contains_chars=true;
            break;
        }
    }
    //..or numbers
    bool contains_numbers = false;
    for(int i=0; i < part.size(); i++){
        QChar current = part.at(i);
        if((current>='0') & (current<='9')){
            contains_numbers=true;
            break;
        }
    }

    if(contains_chars){
        if(contains_numbers){
            *type = TYPE_ALPHANUM;
        }
        else{
            *type = TYPE_STRING;
        }
    }
    else{
        if(contains_numbers){
            *type = TYPE_NUMBER;
        }
        else{
            *type = TYPE_UNKNOWN;
        }
    }

    return true;
}

///
/// \brief analyzeFileName
/// \param file_name
/// \param delims
/// \param parts
/// \return
///
bool splitFileName(const QString& file_name, const QStringList& delims, Scheme* scheme){
    if(scheme == nullptr){
        return false;
    }
    scheme->clear();

    //extract file type
    int last_dot_index = file_name.lastIndexOf('.');
    if(last_dot_index == -1){
        return false;
    }
    QString filetype = file_name.right(file_name.size()-last_dot_index-1);

    //analyze string without filetype
    QString name = file_name.left(last_dot_index);
    int last_delim_idx = 0;
    for(int i = 0; i < name.size(); i++){
        //search for next delimiter
        QChar current = name.at(i);

        for(const QString& delim : delims){
            if(delim.contains(current)){

                if(i-last_delim_idx > 1){
                    //get last name part
                    QString part = name.mid(last_delim_idx,i-last_delim_idx);
                    int type;
                    if(!analyzeSinglePart(part, &type)){
                        return false;
                    }
                    int counter = 0;
                    for(size_t j = 0; j < scheme->types.size(); j++){
                        if(scheme->types[scheme->types.size()-j-1] == type){
                            counter = scheme->counts[scheme->counts.size()-j-1] + 1;
                            break;
                        }
                    }
                    scheme->pushBack(part,type,counter);
                }

                scheme->pushBack(delim, TYPE_DELIM);
                last_delim_idx = i+1;
                break;
            }
        }
    }
    //add last name part
    QString part = name.right(name.size()-last_delim_idx);
    int type;
    if(!analyzeSinglePart(part, &type)){
        return false;
    }
    int counter = 0;
    for(size_t j = 0; j < scheme->types.size(); j++){
        if(scheme->types[scheme->types.size()-j-1] == type){
            counter = scheme->counts[scheme->counts.size()-j-1] + 1;
            break;
        }
    }
    scheme->pushBack(part,type,counter);

    //add filetype to scheme
    scheme->pushBack(".", TYPE_DELIM);
    scheme->pushBack(filetype, TYPE_FILETYPE);
    return true;
}

///
/// \brief schemeToString converts a given scheme to a string for gui output
/// \param scheme
/// \return
///
QString schemeToString(const Scheme& scheme){
    QString result, current;
    for(unsigned int i = 0; i < scheme.parts.size(); i++){
        if(scheme.types[i] == TYPE_DELIM){
            current = scheme.parts[i];
        }
        else if(scheme.types[i] == TYPE_NUMBER){
            current = "<NUMBER_"+QString::number(scheme.counts[i])+">";
        }
        else if(scheme.types[i] == TYPE_STRING){
            current = "<STRING_"+QString::number(scheme.counts[i])+">";
        }
        else if(scheme.types[i] == TYPE_ALPHANUM){
            current = "<ALPHANUM_"+QString::number(scheme.counts[i])+">";
        }
        else if(scheme.types[i] == TYPE_FILETYPE){
            current = "<FILETYPE>";
        }
        else if(scheme.types[i] == TYPE_UNKNOWN){
            current = "<UNKNOWN_"+QString::number(scheme.counts[i])+">";
        }
        else if(scheme.types[i] == TYPE_USERDEFINED){
            current = "\""+scheme.parts[i]+"\"";
        }
        result += current;
    }
    return result;
}

///
/// \brief parseRange
/// \param string
/// \param range
/// \return
///
bool parseRange(const QString& string, std::vector<int>* range){
    if(range == nullptr){
        return false;
    }
    range->clear();

    //check if string starts and ends with number
    if(string.at(0) < '0' || string.at(0) > '9' || string.at(string.size()-1) < '0' || string.at(string.size()-1) > '9'){
        return false;
    }
    //and comma at the end to indicate end of last number range
    QString string_modified = string+",";

    int number_start_idx = 0;
    int range_start = -1;
    int range_end = -1;

    for(int i = 1; i < string_modified.size(); i++){
        //if current char is not a number, it should be a comma or a minus, therefore ending a number
        if(string_modified.at(i) < '0' || string_modified.at(i) > '9'){
            //get current number
            if(i == number_start_idx){//number has zero chars
                return false;
            }
            QString idx = string_modified.mid(number_start_idx, i-number_start_idx);
            bool success;
            int current_number = idx.toInt(&success);
            if(!success){
                return false;
            }
            number_start_idx = i+1;

            //if current is a minus, the current number is the start of range
            if(string_modified.at(i) == '-'){
                if(range_start == -1){
                    range_start = current_number;
                }
                else{
                    //the range was already started
                    return false;
                }
            }
            else if(string_modified.at(i) == ','){//a comma closes a range
                if(range_start == -1){
                    range->push_back(current_number);
                }
                else if(range_end == -1){
                    if(current_number > range_start){
                        for(int j = range_start; j < current_number+1; j++){
                            range->push_back(j);
                        }
                        range_start = -1;
                    }
                    else{
                        return false;
                    }

                }
                else{
                    return false;
                }

            }
            else{
                return false;
            }
        }
    }

    return true;
}

///
/// \brief parseTag
/// \param tag
/// \param scheme
/// \return
///
bool parseTag(const QString& tag, Scheme* scheme){
    if(scheme == nullptr || tag.size() == 0){
        return false;
    }
    int tag_type = TAG_NONE;
    if(tag.at(0) == "<"){
        tag_type = TAG_BRACKET;
    }
    else if(tag.at(0) == "\""){
        tag_type = TAG_QUOTE;
    }

    if(tag_type == TAG_NONE){
        return false;
    }

    QString text = tag.mid(1,tag.size()-2);
    if(tag_type == TAG_QUOTE){
        scheme->pushBack(text,TYPE_USERDEFINED);
    }
    else if(tag_type == TAG_BRACKET && text.size() > 6){
        int type = -1;
        if(text.compare("FILETYPE") == 0){
            scheme->pushBack("",TYPE_FILETYPE);
            return true;
        }

        if(text.left(7).compare("NUMBER_") == 0){
            type = TYPE_NUMBER;
            text = text.right(text.size()-7);
        }
        else if(text.left(7).compare("STRING_") == 0){
            type = TYPE_STRING;
            text = text.right(text.size()-7);
        }
        else if(text.left(9).compare("ALPHANUM_") == 0){
            type = TYPE_ALPHANUM;
            text = text.right(text.size()-9);
        }
        else if(text.left(8).compare("UNKNOWN_") == 0){
            type = TYPE_UNKNOWN;
            text = text.right(text.size()-8);
        }

        if(text.size() == 0){
            return false;
        }

        //extract tag idx, e.g. for <NUMBER_23:1-3,5> extract the 23
        int tag_idx = -1;
        for(int i = 0; i < text.size(); i++){
            if(text.at(i) < '0' || text.at(i) > '9'){
                if(i == 0){
                    //no number found
                    return false;
                }
                else{
                    QString idx = text.left(i);
                    bool success;
                    tag_idx = idx.toInt(&success);
                    if(!success){
                        return false;
                    }
                    text = text.right(text.size()-i);
                    break;
                }
            }
            else if(i == text.size()-1){
                bool success;
                tag_idx = text.toInt(&success);
                if(!success){
                    return false;
                }
                text = "";
            }
        }
        if(tag_idx == -1){
            //no tag number specified
            return false;
        }

        //check if substring of tag is used, e.g. for <NUMBER_23:1-3,5> the :1-3,5 part
        std::vector<int> range;
        if(text.size() > 0){
            if(text.at(0) == ':'){
                text = text.right(text.size()-1);
                if(!parseRange(text,&range)){
                    return false;
                }
            }
            else{
                //wrong format
                return false;
            }
        }

        scheme->pushBack("",type,tag_idx,range);

    }
    else{
        return false;
    }

    return true;
}

///
/// \brief stringToScheme
/// \param string
/// \param scheme
/// \return
///
bool stringToScheme(const QString& string, const QStringList& delims, Scheme* scheme){
    if(scheme == nullptr || string.size() == 0){
        return false;
    }
    scheme->clear();

    int last_separator_idx = 0;
    int tag_type = TAG_NONE;

    if(string.at(0) == "<"){
        tag_type = TAG_BRACKET;
    }
    else if(string.at(0) == "\""){
        tag_type = TAG_QUOTE;
    }

    if(tag_type == TAG_NONE){
        return false;
    }
    tag_type = TAG_NONE;

    for(int i = 0; i < string.size(); i++){
        //search for next delimiter or separator
        QChar current = string.at(i);

        if(current == '<' && i != string.size()-1){
            //check if a new tag can be opened
            if(tag_type == TAG_NONE){
                tag_type = TAG_BRACKET;
                last_separator_idx = i;
            }
            else{
                return false;
            }
            continue;
        }
        else if(current == '>'){
            //check if there currently is a tag open and it has bracket type
            if(tag_type == TAG_BRACKET){
                //get tag and analyze it
                QString tag = string.mid(last_separator_idx,i-last_separator_idx+1);
                if(!parseTag(tag,scheme)){
                    return false;
                }
                tag_type = TAG_NONE;
            }
            else{
                return false;
            }
            continue;
        }
        else if(current == "\""){
            //check if there currently is a tag open and it has quote type
            if(tag_type == TAG_QUOTE){
                QString tag = string.mid(last_separator_idx,i-last_separator_idx+1);
                if(!parseTag(tag,scheme)){
                    return false;
                }
                tag_type = TAG_NONE;
            }
            else if(tag_type == TAG_NONE && i != string.size()-1){
                tag_type = TAG_QUOTE;
                last_separator_idx = i;
            }
            continue;
        }
        else if(tag_type == TAG_NONE){
            if(current == '.'){
                scheme->pushBack(string.mid(i,1),TYPE_DELIM);
            }
            else{
                for(const QString& delim : delims){
                    if(delim.contains(current)){
                        scheme->pushBack(string.mid(i,1),TYPE_DELIM);
                        break;
                    }
                }
            }
            continue;
        }

        if(i == string.size()-1){
            //if this point is reached, none of the conditions before was met, thus the string does not end with a tag closing, i.e. > or "
            return false;
        }

    }
    return true;
}

///
/// \brief match
/// \param analyzed
/// \param target
/// \return
///
bool match(const Scheme& analyzed, const Scheme& target){
    bool match = true;
    for(size_t i = 0; i < target.parts.size(); i++){
        int type = target.types[i];
        if(type == TYPE_NUMBER || type == TYPE_STRING || type == TYPE_ALPHANUM || type == TYPE_FILETYPE || type == TYPE_UNKNOWN){
            int tag_idx = target.counts[i];
            bool found = false;
            for(size_t j = 0; j < analyzed.parts.size(); j++){
                if(analyzed.types[j] == type && analyzed.counts[j] == tag_idx){
                    found = true;
                    break;
                }
            }
            if(!found){
                match = false;
                break;
            }
        }
    }
    return match;
}

///
/// \brief applyScheme
/// \param file_name
/// \param delims
/// \param analyzed
/// \param target
/// \param result
/// \return
///
bool applyScheme(const QString& file_name, const QStringList& delims, const Scheme& analyzed, const Scheme& target, QString* result){
    //assuming the schemes have been matched before
    if(result == nullptr){
        return false;
    }
    //calculate scheme for file name
    Scheme current_scheme;
    if(!splitFileName(file_name, delims, &current_scheme)){
        return false;
    }
    //check if analyzed and current scheme match
    if(current_scheme.parts.size() != analyzed.parts.size()){
        return false;
    }
    for(unsigned int i = 0; i < current_scheme.parts.size(); i++){
        if(current_scheme.types[i] != analyzed.types[i] || current_scheme.counts[i] != analyzed.counts[i]){
            return false;
        }
    }
    //apply target scheme to file name
    for(size_t i = 0; i < target.parts.size(); i++){
        int tag_type = target.types[i];
        int tag_idx = target.counts[i];
        //search for targettag in current scheme
        if(tag_type == TYPE_USERDEFINED || tag_type == TYPE_DELIM){
            *result += target.parts[i];
        }
        else{
            int source_tag_idx = -1;
            for(size_t j = 0; j < current_scheme.parts.size(); j++){
                if(current_scheme.types[j] == tag_type && current_scheme.counts[j] == tag_idx){
                    source_tag_idx = static_cast<int>(j);
                    break;
                }
            }
            if(source_tag_idx == -1){
                return false;
            }

            if(target.filters[i].size() == 0){
                *result += current_scheme.parts[source_tag_idx];
            }
            else{
                for(const int& k : target.filters[i]){
                    if(k >= current_scheme.parts[source_tag_idx].size()){
                        return false;
                    }
                    else{
                        *result += current_scheme.parts[source_tag_idx].mid(k,1);
                    }
                }
            }
        }
    }
    return true;
}



}

#endif // HELPER_H
