#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#pragma once

#include "data.h"

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    ///
    /// \brief changeDir
    /// \param path
    ///
    void changeDir(const QString &path);

private slots:
    ///
    /// \brief selectDir
    ///
    void selectDir();

    ///
    /// \brief openDir
    ///
    void openDir();

    ///
    /// \brief updateFileList
    ///
    void updateFileList();

    ///
    /// \brief updateFileListView
    ///
    void updateFileListView();

    ///
    /// \brief changeSelectionCheckedState
    ///
    void changeSelectionCheckedState();

    ///
    /// \brief deleteCheckedFiles
    ///
    void deleteCheckedFiles();

    ///
    /// \brief setDelimiters
    /// \param idx
    ///
    void setDelimiters(const int idx);

    ///
    /// \brief analyzeFileNames
    ///
    void analyzeFileNames();

    ///
    /// \brief checkTargetScheme
    ///
    void checkTargetScheme();

    ///
    /// \brief showExampleName
    ///
    void showExampleName();

    ///
    /// \brief renameFiles
    ///
    void renameFiles();

private:
    Ui::MainWindow *ui_;
    Data data_;

signals:

    void dirChanged(QString dir_path);
    void updateFiles();
    void updateListView();
};

#endif // MAINWINDOW_H
