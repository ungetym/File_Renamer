#ifndef DATA_H
#define DATA_H
#pragma once

#include <QListWidgetItem>
#include <QObject>

const int SORT_NAME = 0;
const int SORT_TYPE = 1;

const int TYPE_DELIM = 0;
const int TYPE_NUMBER = 1;
const int TYPE_STRING = 2;
const int TYPE_ALPHANUM = 3;
const int TYPE_FILETYPE = 4;
const int TYPE_UNKNOWN = 5;
const int TYPE_USERDEFINED = 6;

const int TAG_NONE = -1;
const int TAG_BRACKET = 0;
const int TAG_QUOTE = 1;

///
/// \brief The Scheme struct represents a file naming scheme
///
struct Scheme{
    std::vector<QString> parts;
    std::vector<int> types;
    std::vector<int> counts;
    std::vector<std::vector<int>> filters;

    void pushBack(const QString& part, const int type, const int count = 0, const std::vector<int>& filter = std::vector<int>()){
        parts.push_back(part);
        types.push_back(type);
        counts.push_back(count);
        filters.push_back(filter);
    }

    void addToFront(const QString& part, const int type, const int count = 0, const std::vector<int>& filter = std::vector<int>()){
        parts.insert(parts.begin(), part);
        types.insert(types.begin(), type);
        counts.insert(counts.begin(), count);
        filters.insert(filters.begin(), filter);
    }

    void clear(){
        parts.clear();
        types.clear();
        counts.clear();
        filters.clear();
    }
};

///
/// \brief The File class extends the QListWidgetItem in order to make a list of files sortable
///
class File : public QListWidgetItem
{
public:
    File(const QString& name);
    QString file_type = "";
    std::vector<QString> splitted_filename;
};

///
/// \brief The Data class holds all data of this project
///
class Data : public QObject
{
    Q_OBJECT
public:
    explicit Data(QObject *parent = nullptr);

    QString dir_path;
    std::vector<File> files;
    int file_sort_method;
    QStringList delimiters = {"_", "-"};

    Scheme analyzed_scheme;
    Scheme target_scheme;

signals:

public slots:
};

#endif // DATA_H
