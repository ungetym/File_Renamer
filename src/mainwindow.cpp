#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "helper.h"

#include <QCollator>
#include <QDesktopServices>
#include <QFileDialog>
#include <QMessageBox>

using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui_(new Ui::MainWindow)
{
    ui_->setupUi(this);
    this->setWindowTitle("File Renamer");

    //connect dir path gui elements
    connect(ui_->lineEdit_dir, &QLineEdit::returnPressed, [&](){changeDir(ui_->lineEdit_dir->text());});
    connect(ui_->button_open, &QPushButton::pressed, this, &MainWindow::openDir);
    connect(ui_->button_select, &QPushButton::pressed, this, &MainWindow::selectDir);
    connect(this, &MainWindow::dirChanged, this, &MainWindow::changeDir);

    //connect view list elements
    connect(this, &MainWindow::updateFiles, this, &MainWindow::updateFileList);
    connect(this, &MainWindow::updateListView, this, &MainWindow::updateFileListView);
    connect(ui_->comboBox_sort, qOverload<int>(&QComboBox::currentIndexChanged), [&](){this->updateFileListView();});
    connect(ui_->button_check, &QPushButton::pressed, this, &MainWindow::changeSelectionCheckedState);
    connect(ui_->button_uncheck, &QPushButton::pressed, this, &MainWindow::changeSelectionCheckedState);
    connect(ui_->checkBox_select_all, &QCheckBox::toggled, this, &MainWindow::changeSelectionCheckedState);
    connect(ui_->button_delete, &QPushButton::pressed, this, &MainWindow::deleteCheckedFiles);

    //connect analyzer elements
    connect(ui_->comboBox_delimiters, qOverload<int>(&QComboBox::currentIndexChanged), this, &MainWindow::setDelimiters);
    connect(ui_->button_analyze, &QPushButton::pressed, this, &MainWindow::analyzeFileNames);
    connect(ui_->lineEdit_target_scheme, &QLineEdit::returnPressed, this, &MainWindow::checkTargetScheme);
    connect(ui_->button_rename, &QPushButton::pressed, this, &MainWindow::renameFiles);
}

MainWindow::~MainWindow(){
    delete ui_;
}

void MainWindow::selectDir(){
    //open file dialog
    QString start_dir = data_.dir_path.size()==0 ? "~" : data_.dir_path;
    QString dir_path = QFileDialog::getExistingDirectory(this,"Choose a directory",start_dir,QFileDialog::ShowDirsOnly);
    //check if a dir was chosen
    if(dir_path.size() == 0){
        return;
    }
    emit dirChanged(dir_path);
}

void MainWindow::openDir(){
    if(data_.dir_path.size() == 0){
        Helper::info("The directory has not been set yet.");
        return;
    }
    else{
        //check if dir exists
        QDir dir(data_.dir_path);
        if(!dir.exists()){
            Helper::info("The directory has not been set yet or does not exist.");
            return;
        }
        else{
            //open dir in default file explorer
            QDesktopServices::openUrl(QUrl::fromLocalFile(data_.dir_path));
        }
    }
}

void MainWindow::changeDir(const QString& path){
    if(path.size() == 0){
        Helper::info("The path could not be set.");
        return;
    }
    //check if dir exists
    QDir dir(path);
    if(!dir.exists()){
        Helper::info("The directory does not exist.");
        return;
    }
    else{
        data_.dir_path = path;
        ui_->lineEdit_dir->setText(path);
        emit updateFiles();
    }
}

void MainWindow::updateFileList(){
    //check if dir exists
    QDir dir(data_.dir_path);
    if(!dir.exists()){
        Helper::info("The directory does not exist.");
        return;
    }

    //get file list
    QStringList file_list = dir.entryList(QDir::Files | QDir::NoDotAndDotDot);

    //sort file list
    QCollator col;
    col.setNumericMode(true);
    col.setCaseSensitivity(Qt::CaseInsensitive);
    std::sort(file_list.begin(), file_list.end(), [&](const QString& a, const QString& b) {return col.compare(a, b) < 0;});

    //set elements
    while(ui_->listWidget->count()>0){
        ui_->listWidget->takeItem(0);
    }
    data_.files.clear();
    for(const QString& file_name : file_list){
        File file(file_name);
        //get file ending
        file.file_type = Helper::getFileType(file_name);
        file.setFlags(file.flags() | Qt::ItemIsUserCheckable);
        file.setCheckState(Qt::Unchecked);
        data_.files.push_back(file);
    }
    data_.file_sort_method = SORT_NAME;

    emit updateListView();
}

void MainWindow::updateFileListView(){
    while(ui_->listWidget->count()>0){
        ui_->listWidget->takeItem(0);
    }

    if(ui_->comboBox_sort->currentText().compare("Name") == 0){
        //check if already sorted according to names
        if(data_.file_sort_method != SORT_NAME){
            vector<File> temp;
            for(const File& file : data_.files){
                for(unsigned int i = 0; i < temp.size(); i++){
                    if(file.text().compare(temp[i].text(),Qt::CaseInsensitive) < 0){
                        temp.insert(temp.begin()+i,file);
                        break;
                    }
                    if(i == temp.size()-1){
                        temp.push_back(file);
                        break;
                    }
                }
                if(temp.size() == 0){
                    temp.push_back(file);
                }
            }
            data_.files = temp;
            data_.file_sort_method = SORT_NAME;
        }
    }
    else{//file types
        //check if already sorted according to file types
        if(data_.file_sort_method != SORT_TYPE){
            vector<File> temp;
            for(const File& file : data_.files){
                for(unsigned int i = 0; i < temp.size(); i++){
                    if(file.file_type.compare(temp[i].file_type,Qt::CaseInsensitive) < 0){
                        temp.insert(temp.begin()+i,file);
                        break;
                    }
                    if(i == temp.size()-1){
                        temp.push_back(file);
                        break;
                    }
                }
                if(temp.size() == 0){
                    temp.push_back(file);
                }
            }
            data_.files = temp;
            data_.file_sort_method = SORT_TYPE;
        }
    }

    //set items to list
    for(File& file : data_.files){
        ui_->listWidget->insertItem(ui_->listWidget->count(), &file);
    }
}

void MainWindow::changeSelectionCheckedState(){
    //get sender of signal and set check state accordingly
    Qt::CheckState check_state;
    QObject* sender = QObject::sender();
    if(sender == ui_->button_check){
        check_state=Qt::Checked;
    }
    else if(sender == ui_->button_uncheck){
        check_state=Qt::Unchecked;
    }
    else{
        check_state = ui_->checkBox_select_all->isChecked() ? Qt::Checked : Qt::Unchecked;
        for(File& file : data_.files){
            file.setCheckState(check_state);
        }
        return;
    }
    //get selected items
    QList<QListWidgetItem*> selection = ui_->listWidget->selectedItems();
    for(QListWidgetItem* item : selection){
        File* file = static_cast<File*>(item);
        file->setCheckState(check_state);
    }
    if(check_state==Qt::Unchecked && selection.size()>0){
        ui_->checkBox_select_all->blockSignals(true);
        ui_->checkBox_select_all->setChecked(false);
        ui_->checkBox_select_all->blockSignals(false);
    }
}

void MainWindow::deleteCheckedFiles(){
    if(!Helper::dialogYesNo("Do you really want to delete the checked files?")){
        return;
    }

    for(unsigned int i = 0; i < data_.files.size(); i++){
        File& file = data_.files[i];
        if(file.checkState() == Qt::Checked){
            //delete file
            QFile qfile(data_.dir_path+"/"+file.text());
            if(!qfile.remove()){
                Helper::info("File "+file.text()+" could not be removed!");
            }
            //delete item
            data_.files.erase(data_.files.begin()+i);
            i--;
        }
    }

    emit updateListView();
}

void MainWindow::setDelimiters(const int idx){
    data_.delimiters.clear();
    if(idx == 0){
        data_.delimiters << "_" << "-" << " ";
    }
    else if(idx == 1){
        data_.delimiters << "_" << "-";
    }
    else if(idx == 2){
        data_.delimiters << "_" << " ";
    }
    else if(idx == 3){
        data_.delimiters << "_";
    }
    else if(idx == 4){
        data_.delimiters << "-" << " ";
    }
    else if(idx == 5){
        data_.delimiters << "-";
    }
    else{
        data_.delimiters << " ";
    }
}

void MainWindow::analyzeFileNames(){
    Scheme scheme;
    for(const File& file : data_.files){
        if(file.checkState() == Qt::Checked){
            //analyze filename
            Scheme current_scheme;
            if(!Helper::splitFileName(file.text(), data_.delimiters, &current_scheme)){
                ui_->lineEdit_analyzed_scheme->setText("Unable to analyze all checked files.");
                return;
            }
            //check if scheme is consistent with previous schemes or set current_scheme as comparison scheme if first
            if(scheme.parts.size() == 0){
                scheme = current_scheme;
            }
            else{
                for(unsigned int i = 0; i < scheme.parts.size(); i++){
                    if(scheme.types[i] != current_scheme.types[i]){
                        ui_->lineEdit_analyzed_scheme->setText("Not all files have the same name structure.");
                        return;
                    }
                }
            }
        }
    }

    data_.analyzed_scheme = scheme;
    ui_->lineEdit_analyzed_scheme->setText(Helper::schemeToString(scheme));
    showExampleName();
}

void MainWindow::checkTargetScheme(){
    QString theme_string = ui_->lineEdit_target_scheme->text();
    Scheme scheme;
    if(Helper::stringToScheme(theme_string, data_.delimiters, &scheme)){
        data_.target_scheme = scheme;
        showExampleName();
    }
    else{
        ui_->display_example->setText("Target scheme invalid!");
    }
}

void MainWindow::showExampleName(){
    if(data_.analyzed_scheme.parts.size() == 0){
        ui_->display_example->setText("No files analyzed to apply the target name scheme.");
        return;
    }
    if(data_.target_scheme.parts.size() == 0){
        ui_->display_example->setText("No target name scheme set to apply to analyzed files.");
        return;
    }
    //check if schemes match
    if(!Helper::match(data_.analyzed_scheme, data_.target_scheme)){
        ui_->display_example->setText("The target name scheme contains tags that are incompatible with the analyzed scheme.");
        return;
    }
    else{
        QString example_source;
        for(const QString& part : data_.analyzed_scheme.parts){
            example_source += part;
        }
        QString example_target;
        if(Helper::applyScheme(example_source, data_.delimiters, data_.analyzed_scheme, data_.target_scheme, &example_target)){
            ui_->display_example->setText(example_source+" -> "+example_target);
        }
        else{
            ui_->display_example->setText("The schemes do not match!");
        }
    }

}

void MainWindow::renameFiles(){
    if(data_.analyzed_scheme.parts.size() == 0){
        Helper::info("No files analyzed to rename.");
        return;
    }
    if(data_.target_scheme.parts.size() == 0){
        Helper::info("No target name scheme set to apply to analyzed files.");
        return;
    }
    //check if schemes match
    if(!Helper::match(data_.analyzed_scheme, data_.target_scheme)){
        Helper::info("The target name scheme contains tags that are incompatible with the analyzed scheme.");
        return;
    }
    else{
        //determine the target names
        bool ignore_warnings = false;
        QStringList target_names;
        for(const File& file : data_.files){
            if(file.checkState() == Qt::Checked){
                QString result;
                if(Helper::applyScheme(file.text(),data_.delimiters,data_.analyzed_scheme, data_.target_scheme, &result)){
                    target_names.push_back(result);
                }
                else{
                    target_names.push_back(file.text());
                    if(!ignore_warnings){
                        if(Helper::dialogYesNo("Some files can't be renamed, e.g. "+file.text()+" . Do you still want to rename all files that can be renamed?")){
                            ignore_warnings = true;
                        }
                        else{
                            Helper::info("Renaming aborted.");
                            return;
                        }
                    }
                }
            }
            else{
                target_names.push_back("");
            }
        }

        ignore_warnings = false;
        for(size_t i = 0; i < data_.files.size(); i++){
            if(data_.files[i].checkState() == Qt::Checked && target_names[i].size() != 0){
                QFile file(data_.dir_path+"/"+data_.files[i].text());
                if(file.exists()){
                    if(!file.rename(data_.dir_path+"/"+target_names[i])){
                        Helper::info("Renaming failed.");
                    }
                }
                else{
                    if(!ignore_warnings){
                        if(Helper::dialogYesNo("Some files do not exist anymore, e.g. "+data_.files[i].text()+" . Do you still want to rename all existing files that can be renamed?")){
                            ignore_warnings = true;
                        }
                        else{
                            Helper::info("Renaming aborted.");
                            return;
                        }
                    }
                }
            }
        }
    }

    emit updateFiles();
}
