QT       += core gui widgets

TARGET = File_Renamer
TEMPLATE = app

CONFIG += c++14
QMAKE_CXXFLAGS += -Wall

DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    data.cpp

HEADERS += \
        mainwindow.h \
    data.h \
    helper.h

FORMS += \
        mainwindow.ui
