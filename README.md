# File Renamer

A helpful tool for systematically renaming multiple files based on QT5.  
Build Instructions: Simply open the project in a current Qt Creator version and build it. A cmake config might be provided later.

Usage:

![GUI Overview](https://gitlab.com/ungetym/File_Renamer/raw/master/images/renamer_00.png "GUI Overview")

1. Select the directory containing the files by pressing "Select".
2. Check the files you want to rename. These should have the same name scheme.
3. Select the delimiters using the drop-down menu.
4. Click "Analyze checked files". If successful, the recognized name scheme is shown, e.g. **\<ALPHANUM_0\>_\<NUMBER_0\>.\<FILETYPE\>** in the image above.
5. Compose your target name scheme, e.g. **\<ALPHANUM_0:0-5\>_\<NUMBER_0:2-4\>.\<FILETYPE\>**, according to the rules below and press Enter. If valid, the example line will show the effect of the target scheme by applying it to the first checked file as shown in the image below.
6. If the example is satisfying, press "Rename" in order to apply the name scheme to all checked files.

![GUI Overview](https://gitlab.com/ungetym/File_Renamer/raw/master/images/renamer_10.png "GUI Overview")

Tagging Rules:

The analyzed tags consist of 4 types, NUMBER, ALPHANUM, STRING and FILETYPE which are followed by a counter, i.e. **\<NUMBER_x\>** refers to the x-th number part of the filename, **\<STRING_y\>** refers to the y-th part of the filename only containing letters and **\<ALPHANUM_z\>** the z-th part containing letters as well as numericals.  
Example of how filenames are analyzed given the delimiters - and _:  
**test23-0001_part_h4X0r-24106.jpeg**  
leads to  
**\<ALPHANUM_0\> - \<NUMBER_0\> _ \<STRING_0\> _ \<ALPHANUM_1\> - \<NUMBER_1\> . \<FILETYPE\>**

Any of the analyzed tags can then be used (even multiple times) in the target name scheme. E.g. the scheme  
**\<ALPHANUM_0\> \<ALPHANUM_1\> - \<NUMBER_0\> . \<FILETYPE\>**  
would rename the example file above to **test23h4X0r-0001.jpeg**

Furthermore, you can specify a part of a tag to be used, e.g.:  
**\<ALPHANUM_0:0-3\>** is **test**  
**\<ALPHANUM_0:2-4\>** is **st23**  
**\<ALPHANUM_0:0-2,4\>** is **tes2**  

And finally you can add new name parts by using quotes, e.g.:
**\<ALPHANUM_0\> _ "myOwnText123" - \<NUMBER_0\> . \<FILETYPE\>**  
renames the example above to  **test23_myOwnText123-0001.jpeg**


Note: The different tag types are not necessary for the currently implemented functionality. However, I plan to include mathmatical operations on NUMBER parts and then the differentiation will be useful. :)

**Update:** The folder additional now contains a nemo action file which can be used in order to add the file renamer to the nemo context menu. Simply change the EXEC path in that file to the file renamer executable location and copy this file to the nemo actions dir, usually ~/.local/share/nemo/actions

For further information or help contact me via <tmi@informatik.uni-kiel.de>